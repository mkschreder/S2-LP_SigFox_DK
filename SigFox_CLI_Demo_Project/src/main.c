/**
* @file    main.c
* @author  AMG & HEA & RF BU
* @version V1.7.1
* @date    Oct, 2020
* @brief   Command line DEMO program
* @details
*
* THE PRESENT FIRMWARE WHICH IS FOR GUIDANCE ONLY AIMS AT PROVIDING CUSTOMERS
* WITH CODING INFORMATION REGARDING THEIR PRODUCTS IN ORDER FOR THEM TO SAVE
* TIME. AS A RESULT, STMICROELECTRONICS SHALL NOT BE HELD LIABLE FOR ANY
* DIRECT, INDIRECT OR CONSEQUENTIAL DAMAGES WITH RESPECT TO ANY CLAIMS ARISING
* FROM THE CONTENT OF SUCH FIRMWARE AND/OR THE USE MADE BY CUSTOMERS OF THE
* CODING INFORMATION CONTAINED HEREIN IN CONNECTION WITH THEIR PRODUCTS.
*
* THIS SOURCE CODE IS PROTECTED BY A LICENSE.
* FOR MORE INFORMATION PLEASE CAREFULLY READ THE LICENSE AGREEMENT FILE LOCATED
* IN THE ROOT DIRECTORY OF THIS FIRMWARE PACKAGE.
*
* <h2><center>&copy; COPYRIGHT 2019 STMicroelectronics</center></h2>
*/

/* Includes ------------------------------------------------------------------*/
#include "st_main.h"

/* Global variables-----------------------------------------------------------*/
static volatile uint8_t interactive = 1;


CommandEntry CommandTable[] = {
  { "help", helpAction, "", "List commands"},
  { "interactive", interactiveAction, "u", "Set interactive mode"},
  { "fw_version", fwVersionAction, "", "Get fw version"},
  { "reboot", rebootAction, "", "reboots the uC"},

  COMMAND_TABLE,
#ifndef MON_REF_DES
  EEPROM_COMMANDS_TABLE,
#endif
  { NULL, NULL, NULL, NULL } // NULL action makes this a terminator
};

/* Functions------------------------------------------------------------------*/
/**
* @brief  Blink the LED indefinitely stucking the application.
* @param  None
* @retval None
*/
static void Fatal_Error(void)
{
  SdkEvalLedInit(LED2);

  while(1)
  {
    SdkDelayMs(100);
    SdkEvalLedToggle(LED2);
  }
}

uint8_t processCmdInput (uint8_t interactive)
{
  static uint8_t buff[COMMAND_BUFFER_LENGTH];
  static uint16_t len;
  static uint16_t currIndex = 0;
  if (interactive) {
    // continue calling emberSerialReadPartialLine() until it returns success, indicating a full line has been read
    if (!serialReadPartialLine((char *)buff, COMMAND_BUFFER_LENGTH, &currIndex)) {
      return 0;
    }

    len=0;
    //search foward looking for first CR, LF, or NULL to determine length of the string
    while((buff[len]!='\n') && (buff[len] !='\r') && (buff[len]!='\0')) {
      len++;
    }
    buff[len ++] = '\r'; //set the final char to be CR
    buff[len ++] = '\n'; //set the final char to be NL

    currIndex = 0;
    return processCommandString(buff, len);
  } else {
    return processCommandInput();
  }
}

void interactiveAction(void)
{
  interactive = unsignedCommandArgument(0);
  responsePrintf("{&N utility call... &t2x}\r\n", "interactive", "mode", interactive);
}

void helpAction(void)
{
  CommandEntry *cmd;

  for (cmd = CommandTable; cmd->name != NULL; cmd++) {
    if(!CMD_HIDDEN(cmd))
    {
      printf ("%s %s - %s\r\n\r\n", cmd->name, cmd->argumentTypes, cmd->description);
    }
  }
}

void fwVersionAction(void)
{
  responsePrintf("{&N API call...&ts}\r\n",
		 "fw_version",
		 "value",FW_VERSION);
}

void rebootAction(void)
{
  responsePrintf("{&N API call...&ts}\r\n",
		 "reboot");
#if  !(defined(BLUENRG2_DEVICE) || defined(BLUENRG1_DEVICE))
  HAL_NVIC_SystemReset();
#else
  NVIC_SystemReset();
#endif
}

/**
* @brief  System main function.
* @param  None
* @retval None
*/
int main(void)
{
  /*Local Variables*/
  ST_SFX_ERR stSfxRetErr;

  /* Init MCU + S2LP MCU configuration*/
  if(ST_Init())
    Fatal_Error();

  /* Wait for SPI CS set to high*/
  SdkDelayMs(10);

  /* Reset S2LP */
  S2LPShutdownEnter();
  SdkDelayMs(10);
  S2LPShutdownExit();

  /* Init the Sigfox Library and the device for Sigfox communication*/
  NVM_BoardDataType sfxConfiguration;
  stSfxRetErr = ST_Sigfox_Init(&sfxConfiguration, 0);

  if(stSfxRetErr != ST_SFX_ERR_NONE)
  {
    /* If an error occured reading Sigfox credentials (for example the board has never been registered)
     * automatically set the test mode credentials. */
    if(stSfxRetErr == ST_SFX_ERR_CREDENTIALS)
    {
	sfxConfiguration.id = 0;
	memset(sfxConfiguration.pac, 0x00, 8);
	sfxConfiguration.rcz = 0;

	set_testMode(1);
    }
    else
    	Fatal_Error();
  }

  /* Set Sigfox configuration for the commands layer */
  set_id(sfxConfiguration.id);
  set_pac(sfxConfiguration.pac);
  set_rcz(sfxConfiguration.rcz);

  /* Calibrate RTC in case of STM32*/
#if  !(defined(BLUENRG2_DEVICE) || defined(BLUENRG1_DEVICE))
  /* The low level driver uses the internal RTC as a timer while the STM32 is in low power.
  This function calibrates the RTC using an auxiliary general purpose timer in order to
  increase its precision. */
  ST_MCU_API_TimerCalibration(500);
#endif

#if  (defined(BLUENRG2_DEVICE) || defined(BLUENRG1_DEVICE))
  SdkEvalComIOConfig(SdkEvalComIOProcessInputData);
#endif

  /* User Application */
  if(get_testMode())
    printf("Sigfox CLI demo - TEST MODE (board not registered)\r\n");
  else
    printf("Sigfox CLI demo\r\n");

  printf("ID: %.8X - PAC: ", sfxConfiguration.id);
  for(uint16_t i = 0; i < sizeof(sfxConfiguration.pac); i++)
  {
    printf("%.2X", sfxConfiguration.pac[i]);
  }
  printf("\r\n");

  while(1)
  {
    /* CLI parser loop */
    if(processCmdInput(interactive))
    {
	if (interactive) {
	  printf(">");
	}
    }

  }
}

#ifdef  USE_FULL_ASSERT
/**
* @brief  Reports the name of the source file and the source line number
*         where the assert_param error has occurred.
* @param file: pointer to the source file name
* @param line: assert_param error line source number
* @retval : None
*/
void assert_failed(uint8_t* file, uint32_t line)
{
  /* User can add his own implementation to report the file name and line number */
  printf("Wrong parameters value: file %s on line %d\r\n", file, line);

  /* Infinite loop */
  while (1)
  {
  }
}
#endif
/******************* (C) COPYRIGHT 2020 STMicroelectronics *****END OF FILE*****/
