/**
******************************************************************************
* @file    BlueNRG1_it.c
* @author  RF Application Team
* @version V1.0.1
* @date    Sep, 2019
* @brief   Main Interrupt Service Routines.
*          This file provides template for all exceptions handler and
*          peripherals interrupt service routine.
******************************************************************************
* @attention
*
* THE PRESENT FIRMWARE WHICH IS FOR GUIDANCE ONLY AIMS AT PROVIDING CUSTOMERS
* WITH CODING INFORMATION REGARDING THEIR PRODUCTS IN ORDER FOR THEM TO SAVE
* TIME. AS A RESULT, STMICROELECTRONICS SHALL NOT BE HELD LIABLE FOR ANY
* DIRECT, INDIRECT OR CONSEQUENTIAL DAMAGES WITH RESPECT TO ANY CLAIMS ARISING
* FROM THE CONTENT OF SUCH FIRMWARE AND/OR THE USE MADE BY CUSTOMERS OF THE
* CODING INFORMATION CONTAINED HEREIN IN CONNECTION WITH THEIR PRODUCTS.
*
* <h2><center>&copy; COPYRIGHT 2015 STMicroelectronics</center></h2>
******************************************************************************
*/

/* Includes ------------------------------------------------------------------*/
#include "BlueNRG1_it.h"
#include "BlueNRG1_conf.h"
#include "SDK_EVAL_Config.h"
#include "S2LP_Middleware_Config.h"
#include "sigfox_stack.h"

#include "clock.h"

__weak void Appli_Exti_CB(uint16_t GPIO_Pin);

/** @addtogroup BlueNRG1_StdPeriph_Examples
* @{
*/

/** @addtogroup SPI_Examples SPI Examples
* @{
*/

/** @addtogroup SPI_Master_Polling SPI Master Polling
* @{
*/

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
/* Private function prototypes -----------------------------------------------*/
/* Private functions ---------------------------------------------------------*/

/******************************************************************************/
/*            Cortex-M0 Processor Exceptions Handlers                         */
/******************************************************************************/

/**
* @brief  This function handles NMI exception.
*/
void NMI_Handler(void)
{
}

/**
* @brief  This function handles Hard Fault exception.
*/
void HardFault_Handler(void)
{
  /* Go to infinite loop when Hard Fault exception occurs */
  while (1)
  {}
}

/**
* @brief  This function handles SVCall exception.
*/
void SVC_Handler(void)
{
}

/******************************************************************************/
/*                 BlueNRG1LP Peripherals Interrupt Handlers                   */
/*  Add here the Interrupt Handler for the used peripheral(s) (PPP), for the  */
/*  available peripheral interrupt handler's name please refer to the startup */
/*  file (startup_BlueNRG1lp.s).                                               */
/******************************************************************************/

/**
* @brief  This function handles PPP interrupt request.
* @param  None
* @retval None
*/
void PPP_IRQHandler(void)
{
}

void GPIO_Handler(void)
{
  if(GPIO_GetITPendingBit(M2S_GPIO_IRQ_PIN)){
    GPIO_ClearITPendingBit(M2S_GPIO_IRQ_PIN);
    handleS2LPIRQ();
  }
}

void Blue_Handler(void)
{

}

void UART_Handler(void)
{
  SdkEvalComIOUartIrqHandler();
}

__weak void USER_DMA_SPI_RX_Callback(void) {};
__weak void USER_DMA_SPI_TX_Callback(void) {};
__weak void USER_DMA_UART_RX_Callback(void) {};
__weak void USER_DMA_UART_TX_Callback(void) {};

void DMA_Handler(void)
{
  /* Check Transfer Complete interrupt */
  if(DMA_GetFlagStatus(DMA_CH_SPI_RX_IT_TC))
  {
    DMA_ClearFlag(DMA_CH_SPI_RX_IT_TC);
    USER_DMA_SPI_RX_Callback();
  }

  if(DMA_GetFlagStatus(DMA_CH_SPI_TX_IT_TC))
  {
    DMA_ClearFlag(DMA_CH_SPI_TX_IT_TC);
    USER_DMA_SPI_TX_Callback();
  }

  if(DMA_GetFlagStatus(DMA_FLAG_TC1))
  {
    DMA_ClearFlag(DMA_FLAG_TC1);
    USER_DMA_UART_TX_Callback();
  }

  if(DMA_GetFlagStatus(DMA_FLAG_TC0))
  {
    DMA_ClearFlag(DMA_FLAG_TC0);
    USER_DMA_UART_RX_Callback();
  }
}

/**
* @}
*/

/**
* @}
*/

/******************* (C) COPYRIGHT 2019 STMicroelectronics *****END OF FILE****/
