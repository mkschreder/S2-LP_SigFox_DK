#include "ST_Init.h"
#include "sigfox_stack.h"
#include <stdarg.h>

/* Declared in stm32lxxx_it.c */
extern volatile uint16_t txUsed;

/*  TIM6 Int Handler, Common for all STM32 Nucleo */
static uint32_t tim6_cnt=0;
TIM_HandleTypeDef  Tim6_Handler={.Instance=TIM6};

int ST_Init(void)
{
  uint8_t nRet = 0;

  /* System initialization function */
  HAL_Init();

  /* System clock initialization */
  ST_MCU_API_SetSysClock();

  /* Put the radio off */
  S2LPShutdownInit();
  SdkDelayMs(10);
  S2LPShutdownExit();

  /* Set the power-saving configuration for GPIOs */
  SetGPIOInitState();

  /* SPI init */
  S2LPSpiInit();

  /* Init UART and TIM6 which will trigger TX */
  SdkEvalComInit();
  SdkEvalTimersTimConfig(&Tim6_Handler, 64-1, 1000-1);
  SdkEvalTimersState(GP_TIMER_ID, &Tim6_Handler, ENABLE);

  /* Set the EEPROM availability */
  S2LPEvalSetHasEeprom(EEPROM_PRESENT);

  /* Auto detect settings, if EEPROM is available */
  if (S2LPEvalGetHasEeprom())
  {
    /* Identify the S2-LP RF board reading some production data */
    S2LPManagementIdentificationRFBoard();
  }
  else
  {
    /* Set XTAL frequency with offset */
    S2LPRadioSetXtalFrequency(XTAL_FREQUENCY+XTAL_FREQUENCY_OFFSET);

    /* Set the frequency base */
    S2LPManagementSetBand(BOARD_FREQUENCY_BAND);

    /* Configure PA availability */
    S2LPManagementSetRangeExtender(DetetctPA());
  }

  /* uC IRQ config and enable */
  S2LPIRQInit();
  S2LPIRQEnable(ENABLE, S2LP_GPIO_IRQ_EDGE_EVENT);

  /* FEM Initialization */
  FEM_Init();

  /* TCXO Initialization */
  TCXO_Init();

  return nRet;
}

uint8_t ButtonInit(void)
{
  int nBtnPressed = 0;
  SdkEvalPushButtonInit(BUTTON_1);

  if(!SdkEvalPushButtonGetState(BUTTON_1))
    nBtnPressed = 1;

  return nBtnPressed;
}

void ButtonSetIRQ(void)
{
  SdkEvalPushButtonIrq(BUTTON_1, BUTTON_MODE_EXTI);
}

uint8_t IsButtonPressed(void)
{
  return SdkEvalPushButtonGetState(BUTTON_1);
}

RangeExtType DetetctPA(void)
{
#if S2LP_FEM_PRESENT == S2LP_FEM_NO
  	return RANGE_EXT_NONE;
#else
#ifdef MON_REF_DES
	return RANGE_EXT_SKYWORKS_SKY66420;
#else
	return RANGE_EXT_CUSTOM;
#endif
#endif
}

void TIM6_IRQHandler(void)
{
  if(__HAL_TIM_GET_IT_SOURCE(&Tim6_Handler, TIM_IT_UPDATE) !=RESET)
  {
    tim6_cnt++;

    SdkEvalComTriggerTx();

    __HAL_TIM_CLEAR_IT(&Tim6_Handler, TIM_IT_UPDATE);
  }
}

void SetGPIOInitState(void)
{
#ifndef DEBUG
  /* For STM32, gpio power consumption is reduced when GPIOs are configured as
  no pull - analog, during this configuration we have to sure thath wake-up pins
  still remain as digital inputs*/

  GPIO_InitTypeDef GPIO_InitStructure;
  GPIO_InitStructure.Mode       = GPIO_MODE_ANALOG;
  GPIO_InitStructure.Pull       = GPIO_NOPULL;
  GPIO_InitStructure.Speed      = GPIO_SPEED_HIGH;

  /* -------------------------------- PORT A ------------------------------------- */
  /* SDN , CS S2-LP, CS E2PROM, UART RX/TX, BUTTON1 */
  GPIO_InitStructure.Pin = GPIO_PIN_All & (~S2LP_M2S_SDN_PIN) & (~S2LP_SPI_CS_PIN) & (~EEPROM_SPI_CS_PIN)
    & (~NUCLEO_UARTx_RX_PIN) & (~SDK_EVAL_UART_TX_PIN) & (~M2S_GPIO_1_PIN) & (~M2S_GPIO_0_PIN) & (~SDK_EVAL_LED1_PIN);
  HAL_GPIO_Init(GPIOA, &GPIO_InitStructure);

  /* -------------------------------- PORT B ------------------------------------- */
  GPIO_InitStructure.Pin = GPIO_PIN_All & (~EEPROM_SPI_XNUCLEO_CS_PIN) & (~M2S_GPIO_2_PIN);
  HAL_GPIO_Init(GPIOB, &GPIO_InitStructure);

  /* -------------------------------- PORT C ------------------------------------- */
  /* IRQ Pin + TCXO Enable */
  GPIO_InitStructure.Pin = GPIO_PIN_All & (~M2S_GPIO_3_PIN) & (~TCXO_EN_PIN);
  HAL_GPIO_Init(GPIOC, &GPIO_InitStructure);

  /* -------------------------------- PORT D ------------------------------------- */
  GPIO_InitStructure.Pin = GPIO_PIN_All;
  HAL_GPIO_Init(GPIOD, &GPIO_InitStructure);
#endif
}

void ST_MANUF_report_CB(uint8_t status, int32_t rssi)
{
  if(status)
    printf("{TEST PASSED! RSSI=%d}\n\r",rssi);
  else
    printf("{TEST FAILED!}\n\r");

  SdkEvalComTriggerTx();
  while(txUsed);
}

#ifdef DEBUG
void ST_dbg_CB(const char *vectcStr,...)
{
  char buffer[256];

  /* Init the UART GPIOs for Nucleo Boards */
  va_list arglist;
  va_start(arglist,vectcStr);
  vsprintf(buffer,vectcStr,arglist);
  va_end(arglist);
  printf(buffer);
  SdkEvalComTriggerTx();
}
#endif
